import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


// 没有标题 只有内容和两个按钮
class CustomTwoButtonDialog extends StatelessWidget {
  const CustomTwoButtonDialog(
      {Key key,
        this.leftButtonTitle,
        this.leftButtonTitleColor,
        this.rightButtonTitle,
        this.rightButtonTitleColor,
        this.content,
        this.padding,
        this.fontSize,
        this.contentColor,
        this.leftOnTap,
        this.rightOnTap})
      : super(key: key);
  final String leftButtonTitle; //左侧按钮文字
  final String rightButtonTitle; //右侧按钮文字
  final String content; //内容
  final double padding; //图片和文字之间的间距
  final double fontSize; //文字的大小
  final Color contentColor; //文字的颜色
  final Color leftButtonTitleColor; //左侧按钮文字的颜色
  final Color rightButtonTitleColor; //右侧按钮文字的颜色
  final leftOnTap; //左侧按钮执行的方法
  final rightOnTap; //右侧按钮执行的方法

  @override
  Widget build(BuildContext context) {
    return _buildCupertinoAlertDialog(context);
  }


  Widget _buildCupertinoAlertDialog(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Container(
        child: CupertinoAlertDialog(
            content: _buildContent(),
            actions: <Widget>[
              GestureDetector(
                child: CupertinoButton(
                    child: Text(leftButtonTitle, style: TextStyle(color: leftButtonTitleColor))
                ),
                onTap: leftOnTap,
              ),
              GestureDetector(
                child: CupertinoButton(
                    child: Text(rightButtonTitle, style: TextStyle(color: rightButtonTitleColor))
                ),
                onTap: rightOnTap,
              )
            ]),
      ),
    );
  }

  Widget _buildContent() {
    return Padding(
      padding: const EdgeInsets.only(top: 18.0),
      child: Column(
        children: <Widget>[
          Text(
            content,
            style: TextStyle(color: contentColor, fontSize: 16),
            textAlign: TextAlign.justify,
          ),
        ],
      ),
    );
  }
}

class ShowCustomOneButtonDialog extends Dialog{
  const ShowCustomOneButtonDialog(
      {Key key,
        this.title,
        this.content,
        this.buttonTitle,
        this.height,
        this.onTap,
        this.context})
      : super(key: key);
  final String title; //文字
  final String content; //内容
  final String buttonTitle; //按钮标题
  final double height;
  final onTap; //按钮点击
  final BuildContext context; //按钮点击

  //一般提示弹框
  tipsDialog() async {
    await showDialog(
        barrierDismissible: false,  // 表示点击灰色背景的时候是否消失弹出框
        context: context,
        builder: (context) {
          return Center(
            child: Container(
              width: 300,
              height: height > 0 ? height : 200,
              //color: Colors.white,
              decoration:BoxDecoration(
                  color: Colors.white,
                  // border: Border.all(
                  //   color: Colors.white,
                  //   width:2.0,
                  // ),
                  borderRadius: BorderRadius.all(Radius.circular(15.0))
              ),
              child: Column(
                children: <Widget>[
                  Container(
                    height: 50,
                    child: Center(
                      child: Text(
                        title,
                        style: TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                            decoration: TextDecoration.none
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 1,child: Divider()),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.only(left: 8, right: 8),
                      child: Center(
                        child: Text(
                          content,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 14,
                              color: Colors.black54,
                              decoration: TextDecoration.none
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 300,
                    child: FlatButton(
                        child: Text('确定',style: TextStyle(fontSize: 16,color: Colors.black)),
                        splashColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        onPressed: onTap
                    ),
                  ),
                ],
              ),
            ),
          );
        }
    );
  }

}

class ShowCustomTwoButtonDialog extends Dialog{
  const ShowCustomTwoButtonDialog(
      {Key key,
        this.title,
        this.content,
        this.onTap,
        this.height,
        this.colors,
        this.context,
        @required this.buttonTitle})
      : super(key: key);
  final String title; //文字
  final String content; //内容
  final String buttonTitle; //按钮标题
  final double height;
  final Color colors;
  final onTap; //按钮点击
  final BuildContext context; //按钮点击

  //一般提示弹框
  tipsDialog() async {
    await showDialog(
        barrierDismissible: false,  // 表示点击灰色背景的时候是否消失弹出框
        context: context,
        builder: (context) {
          return Center(
            child: Container(
              width: 300,
              height: height > 0 ? height : 260,
              //color: Colors.white,
              decoration:BoxDecoration(
                  color: Colors.white,
                  // border: Border.all(
                  //   color: Colors.white,
                  //   width:2.0,
                  // ),
                  borderRadius: BorderRadius.all(Radius.circular(15.0))
              ),
              child: Column(
                children: <Widget>[
                  Container(
                    height: 50,
                    child: Center(
                      child: Text(
                        title,
                        style: TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                            decoration: TextDecoration.none
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 1,child: Divider()),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.only(left: 8, right: 8),
                      child: Center(
                        child: Text(
                          content,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 14,
                              color: colors,
                              decoration: TextDecoration.none
                          ),
                        ),
                      ),
                    ),
                  ),
                  Row(
                    children: [
                      Expanded(
                        flex: 1,
                        child: SizedBox(
                          child: FlatButton(
                              child: Text('取消',style: TextStyle(fontSize: 16,color: Colors.black)),
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              onPressed: () {
                                Navigator.pop(context);
                              }
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 50,
                        child: VerticalDivider(),
                      ),
                      Expanded(
                        flex: 1,
                        child: SizedBox(
                          child: FlatButton(
                              child: Text(buttonTitle,style: TextStyle(fontSize: 16,color: Colors.black)),
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              onPressed: onTap
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        }
    );
  }

}



