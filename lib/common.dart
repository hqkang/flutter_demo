import 'package:flutter/foundation.dart';

class Constant {
  /// debug开关，上线需要关闭
  /// App运行在Release环境时，inProduction为true；当App运行在Debug和Profile环境时，inProduction为false
  static const bool inProduction  = kReleaseMode;

  static bool isDriverTest  = false;
  static bool isUnitTest  = false;

  static const String data = 'data';
  static const String message = 'message';
  static const String code = 'code';

  static const String keyGuide = 'keyGuide';
  static const String phone = 'phone';
  static const String userName = 'userName';
  static const String accessToken = 'accessToken';
  static const String vin = 'vin';
  static const String carSystem = 'carSystem';
  static const String btKey = 'btKey';
  static const String devId = 'devId';
  static const String devName = 'devName';
  static const String userId = 'userId';
  static const String authState = 'authState';
  static const String address = 'address';
  static const String id_card = 'id_card';
  static const String authName = 'authName';
  static const String authPhone = 'authPhone';
  static const String authLicense = 'authLicense';
  static const String refreshToken = 'refreshToken';
  static const String tokenTime = 'tokenTime';

  static const String service_time_end = 'service_time_end';
  static const String theme = 'AppTheme';
  static const String errorCode = 'errorCode';
  static const String carFrameNo = 'carFrameNo';

}