import 'dart:async';
import 'package:date_format/date_format.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_blue/flutter_blue.dart' as ble;
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:flutter_demo/MofiaBluetoothConnection.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter_demo/BleDeviceListEntry.dart';
import 'package:flutter_demo/CtrApp.dart';
import 'package:flutter_demo/ShowDialog.dart';
import 'package:flutter_demo/BluetoothConnection.dart' as demoBle;
import 'package:flutter_demo/convertUtil.dart';
import 'package:provider/provider.dart';


class DeviceChoosePage extends StatefulWidget {
  @override
  _DeviceChoosePageState createState() => _DeviceChoosePageState();
}

// with AutomaticKeepAliveClientMixin<DeviceChoosePage>
class _DeviceChoosePageState extends State<DeviceChoosePage> with AutomaticKeepAliveClientMixin<DeviceChoosePage>{

  DeviceChooseProvider _deviceChooseProvider = new DeviceChooseProvider();
  MofiaBluetoothConnection bluetoothConnection = MofiaBluetoothConnection.blueToothSingleton();
  bool isClickSearch = false;
  ble.BluetoothDevice bluetoothDevice;
  List<ble.ScanResult> scanResultList = List<ble.ScanResult>();
  BluetoothState _bluetoothState = BluetoothState.UNKNOWN;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero).then((value) async {
      ///使用带context的方法
      bluetoothConnection.checkBluetoothTurningOn(context);
    });
    FlutterBluetoothSerial.instance.state.then((state) {
      setState(() {
        _bluetoothState = state;
      });
    });
    // Listen for futher state changes
    FlutterBluetoothSerial.instance
        .onStateChanged()
        .listen((BluetoothState state) {
      setState(() {
        _bluetoothState = state;
      });
    });
  }

  //蓝牙连接结果显示
  void showConnectionResult(String content) {
    ShowDialog().alertDialog(content);
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => _deviceChooseProvider,
      child: Scaffold(
        appBar: new AppBar(
          iconTheme: IconThemeData(
            color: Colors.black, //修改颜色
          ),
          backgroundColor: Colors.white,
          brightness: Brightness.light,
          title: new Text(
            '选择设备',
            style: TextStyle(
                color: Colors.black87
            ),
          ),
          centerTitle: true,
        ),
        body: Container(
          color: Color.fromRGBO(248, 249, 250, 1),
          child: Flex(
            direction: Axis.vertical,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: 50,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: SwitchListTile(
                  title: Text(
                    '打开蓝牙',
                    style: TextStyle(
                        fontSize: 14
                    ),
                  ),
                  value: _bluetoothState.isEnabled,
                  onChanged: (bool value) {
                    // Do the request and update with the true value then
                    future() async {
                      // async lambda seems to not working
                      if (value) {
                        bool isOpenBlue = await FlutterBluetoothSerial.instance
                            .requestEnable();
                        if (isOpenBlue) {
                          print('打开蓝牙成功');
                        } else {
                          print('打开蓝牙失败');
                        }
                      }else{
                        bool isCloseBlue = await FlutterBluetoothSerial.instance.requestDisable();
                        if (isCloseBlue) {
                          print('关闭蓝牙成功');
                        } else {
                          print('关闭蓝牙失败');
                        }
                      }
                    }

                    future().then((_) {
                      setState(() {});
                    });
                  },
                ),
              ),
              Container(
                height: 1,
                margin: EdgeInsets.only(left: 16, right: 16),
                color: Colors.black12,
              ),

              SizedBox(height: 16),
              Selector<DeviceChooseProvider, int>(
                selector: (context, model) => model.searching,
                builder: (context, value, child) {
                  return Container(
                    height: 50,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          padding: EdgeInsets.only(left: 16),
                          child: Text(
                              '可用设备'
                          ),
                        ),
                        Selector<DeviceChooseProvider, int>(
                          selector: (context, model) => model.searching,
                          builder: (context, value, child) {
                            return value == 1
                                ? FittedBox(
                              child: Container(
                                margin: new EdgeInsets.all(16.0),
                                child: CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation<Color>(Colors.black),
                                ),
                              ),
                            )
                                : IconButton(
                              icon: Icon(Icons.replay),
                              iconSize: 30,
                              onPressed: (){
                                _searchBlue();
                              },
                            );
                          },
                        ),

                      ],
                    ),
                  );
                },
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(left: 16, right: 16),
                  child: ListView.builder(
                    itemCount: scanResultList.length,
                    itemBuilder: (BuildContext context, index) {
                      ble.ScanResult result = scanResultList[index];
                      List<int> data = result.advertisementData.manufacturerData[1];
                      String address = '';
                      if (data.length == 10){
                        for (var i = 0; i < 6; i++){
                          List tempList = List();
                          tempList.add(data[5-i]);
                          address = address + ConvertUtil().listTo16String(tempList);
                          if (i != 5){
                            address += ':';
                          }
                        }
                      }else{
                        print('长度不等于10');
                      }
                      return BleDeviceListEntry(
                          device: result.device,
                          address: address,
                          rssi: result.rssi,
                          onTap: () {
                            bluetoothConnection.showConnectionStart(context);
                            print('点击了${result}');
                            List<int> data = result.advertisementData.manufacturerData[1];
                            String address = '';
                            if (data.length == 10){
                              for (var i = 0; i < 6; i++){
                                List tempList = List();
                                tempList.add(data[5-i]);
                                address = address + ConvertUtil().listTo16String(tempList);
                                if (i != 5){
                                  address += ':';
                                }
                              }
                            }else{
                              print('长度不等于10');
                            }

                            print('data: ${data}------address: ${address}');
                            print('${result.advertisementData.manufacturerData}');
                            print('厂商数据: ${ConvertUtil().listTo16String(result.advertisementData.manufacturerData[1])}');
                            bluetoothConnection.blueConnection(address);
                            List tempList = context.read<CtrApp>().logList;
                            if (tempList.length == 0){
                              tempList = List();
                            }
                            tempList.add(formatDate(DateTime.now(), [yyyy, '-', mm, '-', dd, ' ', HH, ':', nn, ':', ss]) + ': ' + '连接蓝牙' + '--${address}');
                            context.read<CtrApp>().setLogList(tempList);
                          },
                          onLongPress: () async {

                          }
                      );
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 80,
                child: Center(
                  child: Container(
                    margin: EdgeInsets.only(left: 16, right: 16, bottom: 8),
                    width: 200,
                    height: 40,
                    color: Colors.transparent,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(16),
                      child: Container(
                        child: FlatButton(
                          color: Colors.black12,
                          onPressed: (){
                            _searchBlue();
                            List tempList = context.read<CtrApp>().logList;
                            if (tempList.length == 0){
                              tempList = List();
                            }
                            tempList.add(formatDate(DateTime.now(), [yyyy, '-', mm, '-', dd, ' ', HH, ':', nn, ':', ss]) + ': ' + '搜索蓝牙');
                            context.read<CtrApp>().setLogList(tempList);
                          },
                          child: Container(
                            height: 40,
                            child: Center(
                              child: Text(
                                '搜索设备',
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }

  Widget getItem(int i) {
    ble.ScanResult result = scanResultList[i];
    ble.AdvertisementData advertisementData = result.advertisementData;
    print('result.name: ${result.device.name}----result: ${result}------result.manufacturerData: ${result.advertisementData.manufacturerData}');
    return Flex(
      direction: Axis.vertical,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        new InkWell(
          child: Container(
            height: 102,
            width: MediaQuery.of(context).size.width - 32,
            decoration: BoxDecoration(
                color: Colors.white60,
                borderRadius: BorderRadius.circular(8.0),
                border: Border.all(
                    width: 1,
                    color: Colors.blue
                )
            ),
            child: Flex(
              direction: Axis.vertical,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 30,
                  padding: EdgeInsets.only(left: 16, top: 8),
                  child: Text(
                    result.device.name.toString(),
                    style: TextStyle(
                      fontSize: 16,
                      color: Color.fromRGBO(35, 38, 44, 1),
                    ),
                  ),
                ),
                Container(
                  height: 30,
                  padding: EdgeInsets.only(left: 16),
                  child: Text(
                    result.device.id.toString(),
                    style: TextStyle(
                      fontSize: 16,
                      color: Color.fromRGBO(35, 38, 44, 1),
                    ),
                  ),
                ),
                Container(
                  height: 40,
                  padding: EdgeInsets.only(left: 16),
                  child: Text(
                    advertisementData.serviceUuids.toString(),
                    style: TextStyle(
                      fontSize: 16,
                      color: Color.fromRGBO(35, 38, 44, 1),
                    ),
                  ),
                )
              ],
            ),
          ),
          onTap: (){

            print('点击了${scanResultList[i]}');
            ble.ScanResult r = scanResultList[i];
            List<int> data = r.advertisementData.manufacturerData[1];
            String address = '';
            if (data.length == 10){
              for (var i = 0; i < 6; i++){
                List tempList = List();
                tempList.add(data[5-i]);
                address = address + ConvertUtil().listTo16String(tempList);
                print('转换后: ${ConvertUtil().listTo16String(tempList)}');
                if (i != 5){
                  address += ':';
                }
              }
            }else{
              print('长度不等于10');
            }

            print('data: ${data}------address: ${address}');
            print('${r.advertisementData.manufacturerData}');
            print('厂商数据: ${ConvertUtil().listTo16String(r.advertisementData.manufacturerData[1])}');
            bluetoothConnection.showWaitDialogStart('正在连接中...');
            bluetoothConnection.blueConnection(address);
          },
        ),
        Container(
          height: 4,
          color: Colors.white,
        )
      ],
    );
  }


  void _searchBlue(){
    print('点击了按钮');
    bluetoothConnection.checkBluetoothTurningOn(context);
    if (bluetoothConnection.blueTurnOn) {
      // bluetoothConnection.startScan();
      if (isClickSearch){
        print('退出');
        return;
      }
      isClickSearch = true;
      _deviceChooseProvider.setSearching(1);
      scanBlue();
    } else {
      Fluttertoast.showToast(msg: '请先开启蓝牙');
      isClickSearch = false;
      return;
    }
    print('执行');

  }

  ///扫描4秒 ,匹配设备，扫到设备后停止扫描
  Future scanBlue() async {
    scanResultList = List<ble.ScanResult>();
    print("开始扫描");
    bluetoothConnection.flutterBlue.startScan(timeout: Duration(seconds: 3));
    await bluetoothConnection.flutterBlue.scanResults.listen((results) {
      if (this.mounted){
        if (results.length > 0){
          setState(() {
            List<ble.ScanResult> tempScanResultList = results;
            for (var i = 0; i < tempScanResultList.length; i++){
              ble.ScanResult scanResult = tempScanResultList[i];
              if (scanResult.device.name.contains('OBD') && !scanResultList.contains(scanResult)){
                scanResultList.add(scanResult);
              }
            }
            // scanResultList = results;
          });
        }
      }
    });
    Future.delayed(Duration(seconds: 3),(){
      isClickSearch = false;
      if (bluetoothConnection.isDiscovering){
        _deviceChooseProvider.setSearching(0);
      }
      setState(() {
      });
    });

  }

}

class DeviceChooseProvider with ChangeNotifier{
  int _networkable=0;
  int _searching = 0; // 0 搜索中  1 搜索完成

  int get networkable => _networkable;
  void setNetworkable(int networkable) {
    _networkable = networkable;
    notifyListeners();
  }

  int get searching => _searching;
  void setSearching(int searching) {
    _searching = searching;
    notifyListeners();
  }

}
