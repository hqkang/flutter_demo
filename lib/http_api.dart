class HttpApi{
  // 域名
  // static const String baseUrl = 'http://mofei.czwok.cn/';
  // static const String baseUrl = 'https://mofei.czwok.cn/';
  static const String baseUrl = 'https://mf.m-fay.com/';
  // static const String baseUrl = 'http://mofei-dev.com/';
  // 图片域名
  // static const String imageBaseUrl = baseUrl + 'api/image/show/';
  static const String imageBaseUrl = '';
  // 获取验证码
  static const String smsCode = baseUrl + 'api/verify';
  // token刷新
  static const String refreshToken = baseUrl + 'api/refreshToken';
  // 登录
  static const String login = baseUrl + 'api/login';
  // 忘记密码
  static const String forgetPassword = baseUrl + 'api/change_pwd';
  // 注册
  static const String register = baseUrl + 'api/register';
  // 登出
  static const String logout = baseUrl + 'api/logout';
  // 文章列表
  static const String articleList = baseUrl + 'api/article/article_lists';
  // 文章分类
  static const String articleCategory = baseUrl + 'api/article/cate_lists';
  // 上传pid数据
  static const String uploadKeyData = baseUrl + 'api/device/receive_key_data';
  // static const String uploadKeyData = 'http://www.edaoduo.com:8031/api/v1/pcapp/keydatas_needprocess';
  // 获取秘钥
  static const String getSecretKey = baseUrl + 'api/btcode/get_code';
  // 获取配钥匙结果
  // static const String getKeyData = 'http://www.edaoduo.com:8031/api/v1/pcapp/result_new';
  static const String getKeyData = baseUrl + 'api/device/get_process_result';
  // 修改头像
  static const String updatePortrait = baseUrl + 'api/identify/headImage';
  // 修改个人信息
  static const String updateUserInfo = baseUrl + 'api/identify/memberEdit';
  // 获取用户信息
  static const String getUserInfo = baseUrl + 'api/identify/memberInfo';
  static const String aliyunFileUpload = 'https://cheyitong.oss-cn-beijing.aliyuncs.com/';
  // 意见反馈提交
  static const String feedback = baseUrl + 'api/feedback/submitted';
  // 文章详情
  static const String articleH5DetailUrl =  baseUrl + 'api/article/detail';
  // 提交实名认证
  static const String authSubmit =  baseUrl + 'api/identify/submitted';
  // 上传原车钥匙ID
  static const String uploadKeyId =  baseUrl + 'api/feedback/car_key_post';
  // 获取原车钥匙ID
  static const String getKeyId =  baseUrl + 'api/feedback/car_key_get';
  // 上传位置信息
  static const String uploadAddress =  baseUrl + 'api/feedback/address_post';
  // 验证蓝牙设备
  static const String bluetoothVerification1 =  baseUrl + 'api/identify/check_sign';
  static const String bluetoothVerification2 =  baseUrl + 'api/identify/check_ic';
  // 版本检测
  static const String check_version =  baseUrl + 'api/identify/check_version';
  // 遥控器验证
  static const String check_key =  baseUrl + 'api/identify/check_contrl';
  // 写成功的遥控器数据上传
  static const String uploadWriteKey =  baseUrl + 'api/identify/contrl_post';
  // 获取车列表
  static const String getCarList =  baseUrl + 'api/article/get_chinese_node';
  // 新匹配列表
  static const String getCarList2 =  baseUrl + 'api/article/get_chinese_node_new';
  // 舒适进入
  static const String getComfortableCarBrandList =  baseUrl + 'api/article/get_chinese_node_new';
  // 获取芯片升级信息
  static const String check_chip_version =  baseUrl + 'api/identify/get_version';
  // 获取排队人数
  static const String get_queue_num =  baseUrl + 'api/feedback/get_queue_num';
  // 获取智能卡列表
  static const String getSmartCardList =  baseUrl + 'api/package/get_package_type';
  // 提交信息
  static const String submitInfo = baseUrl + 'api/feedback/post_delivery';
  // 微信支付
  static const String wechatPay = baseUrl + 'api/pay/wechat_pay';
  // 获取售后反馈
  static const String getAfterSale = baseUrl + 'api/feedback/get_after_sale';
  // 提交售后反馈
  static const String submitAfterSale = baseUrl + 'api/feedback/submitted';
  // 数据校验
  static const String checkData = baseUrl + 'api/device/check_key_data';
  // 舒适进入固件
  static const String getSoftinVersion = baseUrl + 'api/identify/get_softin_version';
  // 舒适进入微信支付
  static const String softinWechatPay = baseUrl + 'api/paysoftin/wechat_pay';
}