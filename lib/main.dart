import 'dart:async';
import 'dart:io';
import 'dart:isolate';
import 'dart:ui';

import 'package:date_format/date_format.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_demo/BluetoothConnection.dart';
import 'package:flutter_demo/MofiaBluetoothConnection.dart';
import 'package:flutter_demo/convertUtil.dart';
import 'package:flutter_demo/custom_dialog.dart';
import 'package:flutter_demo/device_choose_page.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter_demo/CtrApp.dart';
import 'package:flutter_demo/RouterKey.dart';
import 'package:flutter_demo/common.dart';
import 'package:flutter_demo/log_utils.dart';
import 'package:flutter_demo/main_provider.dart';
import 'package:flutter_demo/dio_utils.dart';
import 'package:flutter_demo/http_api.dart';
import 'package:flutter_demo/intercept.dart';
import 'package:flutter_demo/sp_util.dart';
import 'package:provider/provider.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:wifi/wifi.dart';

const debug = true;
String fileName = 'update';
void main() async{
  // MyApp()
  if (Platform.isAndroid){
    await FlutterDownloader.initialize(debug: debug);
  }
  runApp(
      MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => CtrApp()),
        ],
        child: MyApp(),
      )
  );
}


class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  MyApp(){
    print('重新加载');
    Log.init();
    initDio();
  }

  void initDio() {
    final List<Interceptor> interceptors = [];
    /// 统一添加身份验证请求头
    interceptors.add(AuthInterceptor());
    /// 刷新Token
    //interceptors.add(TokenInterceptor());
    /// 打印Log(生产模式去除)
    if (!Constant.inProduction) {
      interceptors.add(LoggingInterceptor());
    }
    /// 适配数据(根据自己的数据结构，可自行选择添加)
    interceptors.add(AdapterInterceptor());
    setInitDio(
      // baseUrl: HttpApi.baseUrl,
      interceptors: interceptors,
    );
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '首页',
      navigatorKey: RouterKey.navigatorKey, //设置在这里
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
      routes: {
        "deviceChoose": (BuildContext context)=>new DeviceChoosePage()
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  MofiaBluetoothConnection bluetoothConnection = MofiaBluetoothConnection.blueToothSingleton();
  List _logList = List();
  MainProvider provider = MainProvider();
  Timer _refreshTimer;
  static ProgressDialog pr;
  List fileDataList = List();
  String urlStr = 'https://cheyitong.oss-cn-beijing.aliyuncs.com/%E8%8A%AF%E7%89%87%E5%8D%87%E7%BA%A7/A3%E8%88%92%E9%80%82%E8%BF%9B%E5%85%A5%E6%96%87%E4%BB%B6/a3_peps_1021.bin';
  File file;
  String fileStr = '';

  final FocusNode _focusNode = FocusNode();
  TextEditingController _firstController;
  TextEditingController _secondController;

  @override
  void dispose() {
    super.dispose();
    if (_refreshTimer != null){
      _refreshTimer.cancel();
      _refreshTimer = null;
    }
  }


  @override
  void initState() {
    super.initState();
    bluetoothConnection.checkBluetoothTurningOn(context);
    _firstController = TextEditingController();
    _secondController = TextEditingController();
    initSp();
    _refreshTimer = Timer.periodic(Duration(seconds: 1), (t) {
      _logList = context.read<CtrApp>().logList;
      setState(() {
      });
    });
    _bindBackgroundIsolate();
  }

  ReceivePort _port = ReceivePort();
  void _unbindBackgroundIsolate() {
    IsolateNameServer.removePortNameMapping('downloader_send');
  }

  void _bindBackgroundIsolate() {
    bool isSuccess = IsolateNameServer.registerPortWithName(
        _port.sendPort, 'downloader_send');
    if (!isSuccess) {
      _unbindBackgroundIsolate();
      _bindBackgroundIsolate();
      return;
    }
    _port.listen((dynamic data) {
      if (debug) {
        print('UI Isolate Callback: $data');
      }
      String id = data[0];
      DownloadTaskStatus status = data[1];
      int progress = data[2];
      setState(() {
        print('test------');
        if (status == DownloadTaskStatus.running) {
          pr.update(progress: progress.toDouble(), message: '下载中,请稍等...');
        }
        if (status == DownloadTaskStatus.failed) {
          fileDataList = List();
          context.read<CtrApp>().setFileDataList(List());
          context.read<CtrApp>().setOriginFileDataList(List());
          ShowCustomTwoButtonDialog(context: context, title: '提示', height: 160, colors: Colors.black54, content: '下载出错,是否重新下载', buttonTitle: '重新下载', onTap: (){
            Navigator.pop(context);
            doUpdate(context, urlStr, '${fileName}.bin');
          }).tipsDialog();
          if (pr.isShowing()) {
            pr.hide();
          }
        }
        if (status == DownloadTaskStatus.complete) {

          if (pr.isShowing()) {
            pr.hide();
          }
          ShowCustomTwoButtonDialog(context: context, title: '提示', colors: Colors.black54, content: '文件已下载', height: 160, buttonTitle: '立即安装', onTap: (){
            Navigator.pop(context);
            // bluetoothConnection.showWaitDialogStart2('正在更新...');
            context.read<CtrApp>().setIsStartUpdate(true);
            context.read<CtrApp>().setCurrentUpdateIndex(0);
            openFile('${fileName}.bin');
            // List tempList = context.read<CtrApp>().logList;
            // if (tempList.length == 0){
            //   tempList = List();
            // }
            // _logList = List.from(tempList);
            // _logList.add(formatDate(DateTime.now(), [yyyy, '-', mm, '-', dd, ' ', HH, ':', nn, ':', ss]) + ': ' + '发送的数据为:' + fileStr);
            // context.read<CtrApp>().setLogList(_logList);
            // bluetoothConnection.packSend();
          },).tipsDialog();
        }
      });

    });
  }

  // 获取file存放地址(外部路径)
  Future<String> get _fileLocalPath async {
    final directory = await getExternalStorageDirectory();
    return directory.path;
  }

  ///3.执行更新操作
  doUpdate(BuildContext context, String url, String fileName) async {
    print('filename: ${fileName}');
    //关闭更新内容提示框
    // Navigator.pop(context);
    //获取权限
    var per = await checkPermission();
    if(per != null && !per){
      print('无权限-------');
      return null;
    }
    //开始下载file
    await executeDownload(context , url, fileName);
  }

  ///4.检查是否有权限
  Future<bool> checkPermission() async {
    //检查是否已有读写内存权限
    PermissionStatus status = await Permission.storage.status;
    print('status--------${status}');

    //判断如果还没拥有读写权限就申请获取权限
    if(status != PermissionStatus.granted){
      var map = await Permission.storage.request();
      print('map: ${map}');
      if(map != PermissionStatus.granted){
        return false;
      }
    }
    return true;
  }

  ///5.下载file
  Future<void> executeDownload(BuildContext context ,String url, String fileName) async {
    //下载时显示下载进度dialog
    if (pr == null){
      pr = new ProgressDialog(context,type: ProgressDialogType.Download, isDismissible: false, showLogs: true);
      if (!pr.isShowing()) {
        pr.show();
      }
    }
    //file存放路径
    final path = await _fileLocalPath;
    print('path: ${path}');
    file = File(path + '/' + fileName);
    print('filepath: ${path + '/' + fileName}');
    // if (await file.exists()) await file.delete();

    FlutterDownloader.registerCallback(downloadCallback);
    //下载
    final taskId = await FlutterDownloader.enqueue(
        url: url,//下载最新file的网络地址
        fileName: fileName,
        savedDir: path,
        showNotification: true,
        openFileFromNotification: true);
  }

  //6.更新
  Future<Null> openFile(String fileName) async {
    String path = await _fileLocalPath;
    // print('file地址: ${path + '/' + fileName}');
    File file = File(path + '/' + fileName);
    List list = await file.readAsBytes();
    fileStr = ConvertUtil().listTo16String(list);
    // fileDataList.add(list);
    context.read<CtrApp>().setFileDataList(fileDataList);
    context.read<CtrApp>().setFileString(fileStr);
    // context.read<CtrApp>().setOriginFileDataList(fileDataList);
    // Log.e('file地址: ${path + '/' + fileName}-----list: ${list}-----list.length: ${list.length}');
  }

  static void downloadCallback(
      String id, DownloadTaskStatus status, int progress) {
    if (debug) {
      print(
          'Background Isolate Callback: task ($id) is in status ($status) and process ($progress)');
    }
    final SendPort send =
    IsolateNameServer.lookupPortByName('downloader_send');
    send.send([id, status, progress]);
    print('id: ${id}--------status: ${status}-------progress: ${progress}---pr: ${pr}');
  }



  Future initSp() async{
    await SpUtil.getInstance();
    SpUtil.putString('isConnect', '0');
    RouterKey.navigatorKey.currentContext.read<CtrApp>().setLogList(List());
    setState(() {
    });
  }

  Widget getItem(int i) {
    return Container(
      color: Color.fromRGBO(245, 245, 245, 1),
      padding: EdgeInsets.all(16),
      child: Text(
          _logList[i]
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return FlutterEasyLoading(
      child: Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text('DEMO'),
          centerTitle: true,
        ),
        body: ChangeNotifierProvider(
          create: (_)=> provider,
          child: GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: (){
              FocusScope.of(context).requestFocus(FocusNode());
            },
            child: Flex(
              // shrinkWrap: true,
              // padding: EdgeInsets.all(0),
              direction: Axis.vertical,
              children: <Widget>[
                SizedBox(height: 8),
                Container(
                  color: Color.fromRGBO(245, 245, 245, 1),
                  child: FlatButton(
                    child: Text('选择设备'),
                    onPressed: () async{
                      await SpUtil.getInstance();
                      List tempList = context.read<CtrApp>().logList;
                      if (tempList.length == 0){
                        tempList = List();
                      }
                      _logList = List.from(tempList);
                      _logList.add(formatDate(DateTime.now(), [yyyy, '-', mm, '-', dd, ' ', HH, ':', nn, ':', ss]) + ': ' + '选择设备');
                      context.read<CtrApp>().setLogList(_logList);
                      Navigator.pushNamed(context, 'deviceChoose');
                    },
                  ),
                ),
                SizedBox(height: 8),
                Container(
                  color: Color.fromRGBO(245, 245, 245, 1),
                  child: FlatButton(
                    child: Text('下载文件'),
                    onPressed: (){
                      // await SpUtil.getInstance();
                      // List tempList = context.read<CtrApp>().logList;
                      // if (tempList.length == 0){
                      //   tempList = List();
                      // }
                      // _logList = List.from(tempList);
                      // _logList.add(formatDate(DateTime.now(), [yyyy, '-', mm, '-', dd, ' ', HH, ':', nn, ':', ss]) + ': ' + '下载文件');
                      // context.read<CtrApp>().setLogList(_logList);
                      doUpdate(context, urlStr, '${fileName}.bin');
                    },
                  ),
                ),
                SizedBox(height: 8),
                Container(
                  color: Color.fromRGBO(245, 245, 245, 1),
                  child: FlatButton(
                    child: Text('初始化'),
                    onPressed: () {
                      // await SpUtil.getInstance();
                      // List tempList = context.read<CtrApp>().logList;
                      // if (tempList.length == 0){
                      //   tempList = List();
                      // }
                      // _logList = List.from(tempList);
                      // _logList.add(formatDate(DateTime.now(), [yyyy, '-', mm, '-', dd, ' ', HH, ':', nn, ':', ss]) + ': ' + '下载文件');
                      // context.read<CtrApp>().setLogList(_logList);
                      // String address = await selfIP();
                      // InternetAddress address =  selfIP;
                      bluetoothConnection.initSocketUtil();
                    },
                  ),
                ),
                SizedBox(height: 8),
                Container(
                  color: Color.fromRGBO(245, 245, 245, 1),
                  child: FlatButton(
                    child: Text('开始'),
                    onPressed: () {
                      // await SpUtil.getInstance();
                      // List tempList = context.read<CtrApp>().logList;
                      // if (tempList.length == 0){
                      //   tempList = List();
                      // }
                      // _logList = List.from(tempList);
                      // _logList.add(formatDate(DateTime.now(), [yyyy, '-', mm, '-', dd, ' ', HH, ':', nn, ':', ss]) + ': ' + '下载文件');
                      // context.read<CtrApp>().setLogList(_logList);
                      // String address = await selfIP();
                      // InternetAddress address =  selfIP;
                      bluetoothConnection.installBegin();
                    },
                  ),
                ),
                Container(
                  height: 100,
                  child: Flex(
                    direction: Axis.horizontal,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Container(
                        width: 260,
                        margin: EdgeInsets.only(left: 16),
                        // color: Colors.blue,
                        child: Flex(
                          direction: Axis.vertical,
                          children: <Widget>[
                            Container(
                              height: 50,
                              // color: Colors.red,
                              child: Container(
                                height: 40,
                                child: Flex(
                                  direction: Axis.horizontal,
                                  children: <Widget>[
                                    Text(
                                      '参数1: '
                                    ),
                                    Container(
                                      height: 40,
                                      width: 200,
                                      child: TextField(
                                          controller: _firstController,
                                          style: TextStyle(color: Colors.blue),
                                          decoration: InputDecoration(
                                            border: OutlineInputBorder(),
                                            labelText: '参数1',
                                          ),
                                          onEditingComplete: () {
                                            print('onEditingComplete');
                                          },
                                          onChanged: (v) {
                                            print('onChanged:' + v);
                                          },
                                          onSubmitted: (v) {
                                            FocusScope.of(context).requestFocus(_focusNode);
                                            print('onSubmitted:' + v);
                                            _firstController.clear();
                                          },
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              height: 50,
                              // color: Colors.red,
                              child: Container(
                                height: 40,
                                child: Flex(
                                  direction: Axis.horizontal,
                                  children: <Widget>[
                                    Text(
                                        '参数2: '
                                    ),
                                    Container(
                                      height: 40,
                                      width: 200,
                                      child: TextField(
                                        controller: _secondController,
                                        style: TextStyle(color: Colors.blue),
                                        decoration: InputDecoration(
                                          border: OutlineInputBorder(),
                                          labelText: '参数2',
                                        ),
                                        onEditingComplete: () {
                                          print('onEditingComplete');
                                        },
                                        onChanged: (v) {
                                          print('onChanged:' + v);
                                        },
                                        onSubmitted: (v) {
                                          FocusScope.of(context).requestFocus(_focusNode);
                                          print('onSubmitted:' + v);
                                          _secondController.clear();
                                        },
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        width: 60,
                        color: Colors.black12,
                        child: FlatButton(
                          child: Text(
                            '发送'
                          ),
                          onPressed: (){
                            bluetoothConnection.packageSend();
                          },
                        ),
                      )
                    ],
                  ),
                ),
                // GridView.count(
                //   padding: EdgeInsets.all(16),
                //   physics: new NeverScrollableScrollPhysics(),//增加
                //   shrinkWrap: true,//增加
                //   crossAxisCount: 4,
                //   mainAxisSpacing: 10,
                //   crossAxisSpacing: 8,
                //   childAspectRatio: 1/0.618,
                //   children: <Widget>[
                //     Container(
                //       color: Color.fromRGBO(245, 245, 245, 1),
                //       child: FlatButton(
                //         child: Text('选择设备'),
                //         onPressed: () async{
                //           await SpUtil.getInstance();
                //           List tempList = context.read<CtrApp>().logList;
                //           if (tempList.length == 0){
                //             tempList = List();
                //           }
                //           _logList = List.from(tempList);
                //           _logList.add(formatDate(DateTime.now(), [yyyy, '-', mm, '-', dd, ' ', HH, ':', nn, ':', ss]) + ': ' + '选择设备');
                //           context.read<CtrApp>().setLogList(_logList);
                //           Navigator.pushNamed(context, 'deviceChoose');
                //         },
                //       ),
                //     ),
                //     Container(
                //       color: Color.fromRGBO(245, 245, 245, 1),
                //       child: FlatButton(
                //         child: Text('发指令'),
                //         onPressed: () async {
                //           List tempList = context.read<CtrApp>().logList;
                //           if (tempList.length == 0){
                //             tempList = List();
                //           }
                //           _logList = List.from(tempList);
                //           _logList.add(formatDate(DateTime.now(), [yyyy, '-', mm, '-', dd, ' ', HH, ':', nn, ':', ss]) + ': ' + '发指令');
                //           context.read<CtrApp>().setLogList(_logList);
                //           String isConnect = await SpUtil.getString('isConnect');
                //           if (isConnect == '0'){
                //             Fluttertoast.showToast(msg: '请先连接蓝牙设备');
                //             List tempList = context.read<CtrApp>().logList;
                //             if (tempList.length == 0){
                //               tempList = List();
                //             }
                //             _logList = List.from(tempList);
                //             _logList.add(formatDate(DateTime.now(), [yyyy, '-', mm, '-', dd, ' ', HH, ':', nn, ':', ss]) + ': ' + '请先连接蓝牙设备');
                //             context.read<CtrApp>().setLogList(_logList);
                //             return;
                //           }
                //           bluetoothConnection.showWaitDialogStart3('发送指令中...');
                //           List tempList2 = context.read<CtrApp>().logList;
                //           if (tempList2.length == 0){
                //             tempList2 = List();
                //           }
                //           _logList = List.from(tempList2);
                //           _logList.add(formatDate(DateTime.now(), [yyyy, '-', mm, '-', dd, ' ', HH, ':', nn, ':', ss]) + ': ' + '发送指令中...');
                //           context.read<CtrApp>().setLogList(_logList);
                //           bluetoothConnection.sendMessage([0x40, 0x00, 0x01, 0x00, 0x23]);
                //         },
                //       ),
                //     ),
                //     Container(
                //       color: Color.fromRGBO(245, 245, 245, 1),
                //       child: FlatButton(
                //         child: Text('扫描'),
                //         onPressed: (){
                //           List tempList = context.read<CtrApp>().logList;
                //           if (tempList.length == 0){
                //             tempList = List();
                //           }
                //           _logList = List.from(tempList);
                //           _logList.add(formatDate(DateTime.now(), [yyyy, '-', mm, '-', dd, ' ', HH, ':', nn, ':', ss]) + ': ' + '扫描');
                //           context.read<CtrApp>().setLogList(_logList);
                //           Navigator.pushNamed(context, 'scan');
                //         },
                //       ),
                //     ),
                //     Container(
                //       color: Color.fromRGBO(245, 245, 245, 1),
                //       child: FlatButton(
                //         child: Text('提交'),
                //         onPressed: (){
                //           List tempList = context.read<CtrApp>().logList;
                //           if (tempList.length == 0){
                //             tempList = List();
                //           }
                //           _logList = List.from(tempList);
                //           _logList.add(formatDate(DateTime.now(), [yyyy, '-', mm, '-', dd, ' ', HH, ':', nn, ':', ss]) + ': ' + '提交');
                //           context.read<CtrApp>().setLogList(_logList);
                //         },
                //       ),
                //     )
                //   ],
                // ),
                Container(
                  height: 40,
                  padding: EdgeInsets.only(left: 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                          '日志'
                      ),
                      FlatButton(
                        child: Text(
                          '清空日志',
                          style: TextStyle(
                              color: Colors.red
                          ),
                        ),
                        onPressed: () async{
                          context.read<CtrApp>().setLogList(List());
                        },
                      )
                    ],
                  ),
                ),
                Expanded(
                  child: Selector<CtrApp, List>(
                    selector: (context, model){
                      _logList = model.logList;
                      return model.logList;
                    },
                    builder: (context, value, child) {
                      return ListView.builder(
                        shrinkWrap: true,
                        itemCount: _logList.length,
                        physics: AlwaysScrollableScrollPhysics(),
                        itemBuilder: (BuildContext context, int position) {
                          return getItem(position);
                        },
                      );
                    },
                  ),
                  // child: ListView.builder(
                  //   shrinkWrap: true,
                  //   itemCount: _logList.length,
                  //   physics: AlwaysScrollableScrollPhysics(),
                  //   itemBuilder: (BuildContext context, int position) {
                  //     return getItem(position);
                  //   },
                  // ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
