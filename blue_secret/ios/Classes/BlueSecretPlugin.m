#import "BlueSecretPlugin.h"
#if __has_include(<blue_secret/blue_secret-Swift.h>)
#import <blue_secret/blue_secret-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "blue_secret-Swift.h"
#endif

@implementation BlueSecretPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftBlueSecretPlugin registerWithRegistrar:registrar];
}
@end
