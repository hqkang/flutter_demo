#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <sys/time.h>
#include <pthread.h>
#include <signal.h>
#include <unistd.h>
#include <jni.h>
#include "package.h"


int clientfd;
pthread_t thread_id[5];

void sighandler(int a) {
    LOGE("触发异常信号: 产生错误! 信号值:%d\n", a);
    exit(-1); //退出进程
}

extern JNIEXPORT void JNICALL Java_com_example_flutter_1plugin_1update_FlutterPluginUpdatePlugin_startServer
        (JNIEnv *env, jobject jobj, jint server_port) {
    int tcp_server_fd;
    int *tcp_client_fd = NULL;
    struct sockaddr_in server_addr;
    struct sockaddr_in client_addr;
    socklen_t tcp_client_addrlen = 0;//存放接收的客户端地址长度
    int tcp_server_port;//服务器端口号

    tcp_server_port = server_port;

    signal(SIGSEGV, sighandler);

    /*1.创建服务器套接字*/
    tcp_server_fd = socket(AF_INET, SOCK_STREAM, 0);

    if (tcp_server_fd < 0) {
        LOGE("Server create error!");
        goto ERROR;
    } else {
        LOGE("Server create successful!");
    }

    //设置缓冲区
    socklen_t optlen; // 选项值长度
    int err;     // 返回值

    int snd_size = 30 * 1024;
    optlen = sizeof(snd_size);
    err = setsockopt(tcp_server_fd, SOL_SOCKET, SO_SNDBUF, (const char *) &snd_size, optlen);
    if (err < 0) {
        LOGE("服务器:设置发送缓冲区大小错误");
    }

    int rcv_size = 30 * 1024;
    optlen = sizeof(rcv_size);
    err = setsockopt(tcp_server_fd, SOL_SOCKET, SO_RCVBUF, (const char *) &rcv_size, optlen);
    if (err < 0) {
        LOGE("服务器:设置接收缓冲区大小错误");
    }

    memset(&server_addr, 0, sizeof(struct sockaddr_in));

    /*2.绑定端口*/
    server_addr.sin_family = AF_INET;   //ipv4
    server_addr.sin_port = htons(tcp_server_port); //大端转小端
    server_addr.sin_addr.s_addr = INADDR_ANY;  //本地IP地址赋值
    LOGE("server connection: %s:%d\n", inet_ntoa(server_addr.sin_addr),
         ntohs(server_addr.sin_port));

    if (bind(tcp_server_fd, (const struct sockaddr *) &server_addr, sizeof(struct sockaddr)) < 0) {
        LOGE("The server port binding fails!");
        goto ERROR;
    }

    /*3.设置监听的客户端数量*/
    if (listen(tcp_server_fd, 10)) {
        LOGE("Description Failed to set the listening number!");
        goto ERROR;
    }

    while (1) {
        tcp_client_addrlen = sizeof(struct sockaddr);
        tcp_client_fd = malloc(sizeof(int));
        *tcp_client_fd = accept(tcp_server_fd, (struct sockaddr *) &client_addr,
                                &tcp_client_addrlen);
        if (*tcp_client_fd < 0) {
            LOGE("Wait for the client connection to fail!");
        } else {
            if (clientfd != 0) {
                close(clientfd);
            }
            //打印连接的客户端信息
            LOGE("client connection: %s:%d\n", inet_ntoa(client_addr.sin_addr),
                 ntohs(client_addr.sin_port));
            if (pthread_create(&thread_id[0], NULL, socket_pthread_func, (void *) tcp_client_fd) ==
                0) {
                //设置线程分离属性
                pthread_detach(thread_id[0]);
            }
        }
    }

    ERROR:
    close(tcp_server_fd);
}