#include <string.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <float.h>
#include <limits.h>
#include <ctype.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <sys/time.h>
#include <unistd.h>
#include <time.h>
#include "cJSON.h"
#include "package.h"
#include "update.h"

extern int clientfd;
extern struct SocketPackageData thread_datapack[5];
extern uint8_t update_file[1024 * 200];
extern uint32_t filelen;

static void
pack_ble_to_json(struct SocketPackageData *jsonpackdata, struct BlePackageData *blepackdata,
                 int nLen) {
    HexToStr(&jsonpackdata->Data[0], (uint8_t *) &blepackdata->Head, 1);
    HexToStr(&jsonpackdata->Data[2], (uint8_t *) &blepackdata->PackNum, 1);
    HexToStr(&jsonpackdata->Data[4], blepackdata->Data, nLen);
}

void *pthread_update_func(void *arg) {
    char *csend;
    int send_len = 0;
    int timeout = 0;
    struct BlePackageData blepackdata;
    struct SocketPackageData jsonpackdata;
//    int filelen = strlen(update_file);
    LOGE("filelen=%d\n", filelen);
    memset(&blepackdata, 0, sizeof(struct BlePackageData));
    memset(&jsonpackdata, 0, sizeof(struct SocketPackageData));
    pthread_t id = pthread_self();
    LOGE("线程1的id=%lu\n", id);
    while (1) {
        if (thread_datapack[1].Type == EM_UPDATE) {
            //判断是否有文件
            if (filelen != 0) {
                //判断文件是否已经发完
                if (filelen != send_len) {
                    if ((filelen - send_len) < 128) {
                        //只够一包数据
                        if (send_len == 0) {
                            blepackdata.Head = 0x05;
                            blepackdata.PackNum = 0;
                        } else {
                            memset(blepackdata.Data, 0, 128);
                            blepackdata.PackNum++;
                        }
                        memcpy(blepackdata.Data, update_file, (filelen - send_len) * sizeof(uint8_t));
                        send_len += (filelen - send_len);
                        pack_ble_to_json(&jsonpackdata,&blepackdata,filelen - send_len);
                    } else {
                        //第一包数据
                        if (send_len == 0) {
                            blepackdata.Head = 0x05;
                            blepackdata.PackNum = 0;
                        } else {
                            blepackdata.PackNum++;
                        }
                        memcpy(blepackdata.Data, &update_file[send_len], 128 * sizeof(uint8_t));
                        send_len += 128;
                        pack_ble_to_json(&jsonpackdata,&blepackdata,128);
                    }
                    if (blepackdata.PackNum > 0x7F) {
                        blepackdata.PackNum = 0;
                    }

                    jsonpackdata.Status = 0;
                    csend = send_json(jsonpackdata);
                    LOGE("发送数据=%s\n", csend);
                    send(clientfd, csend, strlen(csend), 0);
                } else {
                    LOGE("升级文件更新完毕");
                    break;
                }
            } else {
                LOGE("无升级文件");
                break;
            }
        } else {
//            if()
        }
        sleep(1);
    }

    LOGE("线程1退出");
    pthread_exit(NULL);
}

void *pthread_reg_func(void *arg) {
    char *csend;
    pthread_t id = pthread_self();
    LOGE("线程2的id=%lu\n", id);

    while (1) {
        if (thread_datapack[2].Type == 2) {
            csend = send_json(thread_datapack[2]);
            send(clientfd, csend, strlen(csend), 0);
            memset(&thread_datapack[2], 0, sizeof(struct SocketPackageData));
        }

        sleep(1);
    }

    LOGE("线程2退出");
    pthread_exit(NULL);
}