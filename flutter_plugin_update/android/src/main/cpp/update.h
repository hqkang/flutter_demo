
#ifndef __UPDATE__H
#define __UPDATE__H

#ifdef __cplusplus
extern "C"
{
#endif

#pragma pack(1)
//定义蓝牙文件更新数据传输的帧结构体
struct BlePackageData {
    uint16_t Head;      // 0x05表示更新模块 0x01表示查询SFD
    uint16_t PackNum;   //表示消息的序号
    uint8_t  Data[300]; //表示CAN传输数据
};
#pragma pack()

extern void *pthread_update_func(void *arg);

extern void *pthread_reg_func(void *arg);

#ifdef __cplusplus
}
#endif

#endif
