
#ifndef __PACKAGE__H
#define __PACKAGE__H

#include <android/log.h>

#ifdef __cplusplus
extern "C"
{
#endif

#define  LOG_TAG    "socket_server"
#define  LOGI(FORMAT, ...) __android_log_print(ANDROID_LOG_INFO,LOG_TAG,FORMAT,##__VA_ARGS__)
#define  LOGE(FORMAT, ...) __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,FORMAT,##__VA_ARGS__)
#define  LOGD(FORMAT, ...)  __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG,FORMAT, ##__VA_ARGS__)

#define MAX_DATA (1024*190)    //每帧最大传输大小

#pragma pack(1)
//定义socket数据传输的结构体
struct SocketPackageData {
    int Type;                    //功能码
    int NumCnt;                  //Package编号
    uint8_t Data[MAX_DATA];      //Package数据
    int DataSize;                //数据总大小
    int Status;                  //状态
};
#pragma pack()

enum Funstype {
    EM_DOWNLODE = 1,
    EM_UPDATE = 2,
};

extern void receive_json_to_str(char *data, struct SocketPackageData *datapack);

extern char *send_json(struct SocketPackageData datapack);

extern void *socket_pthread_func(void *arg);
extern void HexToStr(uint8_t *pbDest, uint8_t *pbSrc, int nLen);

#ifdef __cplusplus
}
#endif

#endif
