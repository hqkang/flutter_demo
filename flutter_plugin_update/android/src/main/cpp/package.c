#include <string.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <float.h>
#include <limits.h>
#include <ctype.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <sys/time.h>
#include <unistd.h>
#include "cJSON.h"
#include "package.h"
#include "update.h"

#define READ_LEN (1024)
#define READ_MAX (1024*200)
uint8_t update_file[1024 * 200];
uint32_t filelen;
struct SocketPackageData thread_datapack[5];//定义接收包
extern int clientfd;
extern pthread_t thread_id[5];

void StrToHex(uint8_t *pbDest, uint8_t *pbSrc, int nLen) {
    uint8_t h1, h2;
    uint8_t s1, s2;
    int i;
    for (i = 0; i < nLen; i++) {
        h1 = pbSrc[2 * i];
        h2 = pbSrc[2 * i + 1];

        s1 = h1 - 0x30;
        if (s1 > 9)
            s1 -= 7;
        s2 = h2 - 0x30;
        if (s2 > 9)
            s2 -= 7;
        pbDest[i] = s1 * 16 + s2;
    }
}

void HexToStr(uint8_t *pbDest, uint8_t *pbSrc, int nLen) {
    uint8_t ddl, ddh;
    int i;

    for (i = 0; i < nLen; i++) {
        ddh = 48 + pbSrc[i] / 16;
        ddl = 48 + pbSrc[i] % 16;
        if (ddh > 57) ddh = ddh + 7;
        if (ddl > 57) ddl = ddl + 7;
        pbDest[i * 2] = ddh;
        pbDest[i * 2 + 1] = ddl;
    }
    pbDest[nLen * 2] = '\0';
}

void receive_json_to_str(char *data, struct SocketPackageData *datapack) {
    cJSON *json;
    cJSON *item;
    uint8_t data_buff[MAX_DATA];

    memset(datapack, 0, sizeof(struct SocketPackageData));

    json = cJSON_Parse(data);
    if (!json) {
        LOGE("Error before: [%s]\n", cJSON_GetErrorPtr());
    } else {
        item = cJSON_GetObjectItem(json, "Type");
        datapack->Type = item->valueint;
        item = cJSON_GetObjectItem(json, "NumCnt");
        datapack->NumCnt = item->valueint;
        item = cJSON_GetObjectItem(json, "Data");
        memcpy(data_buff, item->valuestring, strlen(item->valuestring));
        StrToHex(datapack->Data, data_buff, (int)(strlen(item->valuestring) / 2));
        item = cJSON_GetObjectItem(json, "DataSize");
        datapack->DataSize = item->valueint;
        item = cJSON_GetObjectItem(json, "Status");
        datapack->Status = item->valueint;
    }
    cJSON_Delete(json);
}

int analyze_packdata(struct SocketPackageData datapack) {
    LOGE("长度2=%d", datapack.DataSize);
    LOGE("数据为%x\n", datapack.Data[0]);
    LOGE("数据为%x\n", datapack.Data[1]);
    LOGE("数据为%x\n", datapack.Data[2]);

    switch (datapack.Type) {
        case EM_DOWNLODE:
            memset(update_file, 0, sizeof(update_file) / sizeof(update_file[0]));
            filelen = datapack.DataSize;
            memcpy(update_file, datapack.Data, filelen*(sizeof(uint8_t)));

            LOGE("文件内容为%d\n", update_file[0]);
            LOGE("文件内容为%d\n", update_file[1]);
            LOGE("文件内容为%d\n", update_file[2]);
            break;
        case EM_UPDATE:
            if (thread_id[1] == 0) {
                if (pthread_create(&thread_id[1], NULL, pthread_update_func, NULL) ==
                    0) {
                    LOGE("创建线程1成功");
                    //设置线程分离属性
                    pthread_detach(thread_id[1]);
                }
            }
            memset(&thread_datapack[1], 0, sizeof(struct SocketPackageData));
            thread_datapack[1] = datapack;
            break;
        case 6:
            if (thread_id[2] == 0) {
                if (pthread_create(&thread_id[2], NULL, pthread_reg_func, NULL) ==
                    0) {
                    LOGE("创建线程2成功");
                    //设置线程分离属性
                    pthread_detach(thread_id[2]);
                }
            }
            thread_datapack[2] = datapack;
            break;
        default:
            break;
    }

    return 0;
}


char *send_json(struct SocketPackageData datapack) {
    cJSON *json = cJSON_CreateObject();
    char *in;

    cJSON_AddNumberToObject(json, "Type", datapack.Type);
    cJSON_AddNumberToObject(json, "NumCnt", datapack.NumCnt);
    cJSON_AddStringToObject(json, "Data", datapack.Data);
    cJSON_AddNumberToObject(json, "DataSize", datapack.DataSize);
    cJSON_AddNumberToObject(json, "Status", datapack.Status);
    in = cJSON_PrintUnformatted(json);
    cJSON_Delete(json);
    return in;
}

void *socket_pthread_func(void *arg) {
    LOGE("接收线程创建成功!");
    clientfd = *(int *) arg;
    int rx_cnt;
    char rxbuff[1024 * 200];
    int rxbuf_len = 0;
    fd_set readfds;
    struct timeval timeout;  //select超时时间值
    int select_state; //接收返回值
    int timeout_cnt = 0;//超时计数
    struct SocketPackageData rx_datapack;//临时接收数据包
    free(arg);
    pthread_t id = pthread_self();
    LOGE("thread_id=%lu\n", id);
    while (1) {
        FD_ZERO(&readfds);
        FD_SET(clientfd, &readfds);
        timeout.tv_sec = 10;  //超时
        timeout.tv_usec = 0;
        select_state = select(clientfd + 1, &readfds, NULL, NULL, &timeout);
        if (select_state > 0) {
            //是否产生了事件
            if (FD_ISSET(clientfd, &readfds)) {
                rx_cnt = recv(clientfd, &rxbuff[rxbuf_len], READ_LEN, 0);
                if (rx_cnt == 0) {
                    LOGE("read empty,break");
                    break;
                }

                //心跳包处理
                if (rxbuff[0] == 'A') {
                    send(clientfd, &rxbuff[0], 1, 0);
                    timeout_cnt = 0;
                    rxbuf_len = 0;
                }

                if (rxbuff[0] != '{') {
                    rxbuf_len = 0;
                } else {
                    rxbuf_len += rx_cnt;
                    if ((rxbuff[rxbuf_len - 1]) != '}') {
                        //超出最大接收
                        if ((rxbuf_len + READ_LEN) > READ_MAX) {
                            rxbuf_len = 0;
                        }
                    } else {
                        LOGE("%s\n", rxbuff);
                        receive_json_to_str(rxbuff, &rx_datapack);
                        rxbuf_len = 0;
                        analyze_packdata(rx_datapack);
                    }
                }
            }
        } else if (select_state == 0) {
            timeout_cnt++;
            LOGE("Timeout!cnt=%d", timeout_cnt);
            if (timeout_cnt < 5) {
                continue;
            } else
                break;
        } else if (select_state < 0) {
            LOGE("Select error!");
            break;
        }
    }

    shutdown(clientfd, SHUT_RDWR);
    // close(clientfd);
    pthread_exit(NULL);
}
