//package android.src.main.java.com.example.flutter_plugin_update;
package com.example.flutter_plugin_update;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;
import io.flutter.plugin.common.PluginRegistry.Registrar;

import androidx.annotation.NonNull;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.Random;
import com.google.gson.Gson;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

/**
 * FlutterPluginUpdatePlugin
 */
public class FlutterPluginUpdatePlugin implements FlutterPlugin, MethodCallHandler {
    /// The MethodChannel that will the communication between Flutter and native Android
    ///
    /// This local reference serves to register the plugin with the Flutter Engine and unregister it
    /// when the Flutter Engine is detached from the Activity
    private MethodChannel channel;

    static {
        System.loadLibrary("socket_server");
    }

    public static native void startServer(int port);

    public static void registerWith(Registrar registrar) {
        final MethodChannel channel = new MethodChannel(registrar.messenger(), "flutter_plugin_update");
        channel.setMethodCallHandler(new FlutterPluginUpdatePlugin());
    }

    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
        channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "flutter_plugin_update");
        channel.setMethodCallHandler(this);
    }

    @Override
    public void onMethodCall(@NonNull MethodCall call, final @NonNull Result result) {
        switch (call.method) {
            case "getPlatformVersion":
                result.success("Android " + android.os.Build.VERSION.RELEASE);
                break;

            case "installBegin" :
                getInstance().handleBluetoothData("{\"Type\":2,\"NumCnt\":0,\"Data\":\"123\",\"CurrentSize\":1024,\"DataSize\":3,\"Status\":0}");

                break;
            case "startUpdate":

                String data = call.argument("data");

                getInstance().startUpdate(data,
                        new SocketCallback() {
                    @Override
                    public void sendToDevice(String data) {
                        result.success(data);
                    }
                });

                break;
            case "initSocketUtil":
                String ip = call.argument("ip");
                System.out.println("ip: " + ip);
                getInstance().init(ip);
                break;
            default:
                result.notImplemented();
                break;
        }

//        if (call.method.equals()) {
//            result.success("Android " + android.os.Build.VERSION.RELEASE);
//        } else {
//            result.notImplemented();
//        }
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        channel.setMethodCallHandler(null);
    }

    public static UpdateSocketUtil getInstance() {
        return UtilHolder.instance;
    }

    private static class UtilHolder {
        private static UpdateSocketUtil instance = new UpdateSocketUtil();
    }

    private static String mIp = "";

    public static class UpdateSocketUtil {

        static {
            System.loadLibrary("socket_server");
        }

        private int port = 0;

        private static final String TAG = "UpdateSocket";

        private SocketCallback mCallback;

        private Socket socket;

        private UpdateSocketUtil.WriteThread writeThread;
        private UpdateSocketUtil.ReadThread readThread;

        private UpdateSocketUtil() {
        }

        public void init(final String ip) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    UpdateSocketUtil updateSocketUtil = getInstance();
                    Random r = new Random();
                    port = r.nextInt(9999);
                    mIp = ip;

//                    Log.e(TAG, port + "");
                    System.out.println("port = "+ port);
                    startServer(port);

                }
            }).start();
        }

        // 开始升级
        public void startUpdate(final String data, SocketCallback callback) {
            this.mCallback = callback;

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        System.out.println(TAG + "连接c服务器..");
                        //取连接客户端
                        socket = new Socket();

                        socket.connect(new InetSocketAddress(mIp, port));

                        writeThread = new UpdateSocketUtil.WriteThread(socket, data);
                        writeThread.start();

                        if (null == readThread) {
                            readThread = new UpdateSocketUtil.ReadThread(socket);
                            readThread.start();
                        }

                        if (socket.isConnected()) {

                            while (true) {
                                // 读数据

                                InputStream ips = socket.getInputStream();

                                InputStreamReader ipsr = new InputStreamReader(ips, "UTF-8");

                                char[] chars = new char[512];
                                ipsr.read(chars, 0, chars.length);
                                Log.e(TAG, "接收的全部 Json  —— " + String.valueOf(chars));


                                for (int i = 0; i < chars.length; i++) {
                                    if (String.valueOf(chars[i]).equals("}")) {
                                        int valuePlace = i + 1;
                                        Log.e(TAG, "接收的 place —— " + valuePlace);
                                        if (valuePlace < 5) break;
                                        char[] realValue = new char[valuePlace];
                                        System.arraycopy(chars, 0, realValue, 0, valuePlace);
                                        Log.e(TAG, "接收的 realValue —— \n" + String.valueOf(realValue));

                                        try {
                                            Gson gson = new Gson();
                                            JsonData data = gson.fromJson(String.valueOf(realValue), JsonData.class);
                                            Log.e(TAG, "JsonData —— data:" + String.valueOf(data.Data));
                                            mCallback.sendToDevice(String.valueOf(data.Data));

                                        } catch (Exception e) {
                                            Log.e(TAG, "error —— " + e);
                                        }
                                        break;
                                    }
                                }

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }

        // 处理来着蓝牙设备的数据
        public void handleBluetoothData(final String bluetoothData) {
            try {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            System.out.println(TAG + "向c服务器写数据===");
                            writeThread.out.write(bluetoothData.getBytes("UTF-8"));
                            writeThread.out.flush();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        static class WriteThread extends Thread {
            DataOutputStream out = null;
            String mData = "";

            public WriteThread(Socket socket, String data) {
                try {
                    out = new DataOutputStream(socket.getOutputStream());
                    mData = data;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void run() {
                super.run();
                try {
                    Log.e(TAG, "向c服务器写数据===");

                    JsonData jsonData = new JsonData();
                    jsonData.Data = mData;
                    jsonData.DataSize = mData.length();
                    jsonData.NumCnt = 0;
                    jsonData.Type = 1;
                    Gson gson = new Gson();
                    String jsonValue = gson.toJson(jsonData);

                    Log.e(TAG, "length = " + jsonValue.length());
                    byte[] v = jsonValue.getBytes("UTF-8");
                    out.write(v);
                    out.flush();
                    Log.e(TAG, jsonValue);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        static class ReadThread extends Thread {
            BufferedReader bufferedReader;
            DataOutputStream out;

            public ReadThread(Socket socket) {
                try {
                    bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    out = new DataOutputStream(socket.getOutputStream());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void run() {
                super.run();
                //循环读取内容
                while (true) {
                    try {
                        sleep(3000);
//                    byte d = 0x1A;
                        byte[] v = "A".getBytes("UTF-8");
                        out.write(v);
                        out.flush();
                        Log.e("beat", "发送心跳：" + "A");
                    } catch (Exception e) {
                        Log.e(TAG, "ReadThread error ———— " + e.toString());
                    }
                }
            }
        }

    }

    public static String getIpAddress(Context context) {
        NetworkInfo info = ((ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();

        if (info != null && info.isConnected()) {
            // 3/4g网络
            if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
                try {
                    for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                        NetworkInterface intf = en.nextElement();
                        for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                            InetAddress inetAddress = enumIpAddr.nextElement();
                            if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                                return inetAddress.getHostAddress();
                            }
                        }
                    }
                } catch (SocketException e) {
                    e.printStackTrace();
                }

            } else if (info.getType() == ConnectivityManager.TYPE_WIFI) {
                //  wifi网络
                WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                String ipAddress = intIP2StringIP(wifiInfo.getIpAddress());
                return ipAddress;
            } else if (info.getType() == ConnectivityManager.TYPE_ETHERNET) {
                // 有限网络
                return getLocalIp();
            }
        }
        return null;
    }

    private static String intIP2StringIP(int ip) {
        return (ip & 0xFF) + "." +
                ((ip >> 8) & 0xFF) + "." +
                ((ip >> 16) & 0xFF) + "." +
                (ip >> 24 & 0xFF);
    }


    // 获取有限网IP
    private static String getLocalIp() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface
                    .getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf
                        .getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()
                            && inetAddress instanceof Inet4Address) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (SocketException ex) {

        }
        return "0.0.0.0";
    }

    static class JsonData {
        public int Type;                    //功能码
        public int NumCnt;                  //Package编号
        public String Data;                 //Package数据
        public int CurrentSize;             //当前Package大小
        public int DataSize;                //数据总大小
        public int Status;                  // 状态 0 —— 正常，1 —— 异常
    }

    public interface SocketCallback {
        void sendToDevice(String data);

//        void updateMessage();
    }
}
